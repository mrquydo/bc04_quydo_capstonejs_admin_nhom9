function layThongTinTuForm() {
  let newProduct = {
    name: document.getElementById("name").value,

    price: document.getElementById("price").value,

    screen: document.getElementById("screen").value,

    backCamera: document.getElementById("backCamera").value,

    frontCamera: document.getElementById("frontCamera").value,

    image: document.getElementById("image").value,

    desc: document.getElementById("desc").value,

    type: document.getElementById("type").value,
  };
  return newProduct;
}

function showThongTinLenForm(sp) {
  document.getElementById("name").value = sp.name;
  document.getElementById("price").value = sp.price;
  document.getElementById("screen").value = sp.screen;
  document.getElementById("backCamera").value = sp.backCamera;
  document.getElementById("frontCamera").value = sp.frontCamera;
  document.getElementById("image").value = sp.image;
  document.getElementById("desc").value = sp.desc;
  document.getElementById("type").value = sp.type;
}
function clearForm() {
  document.getElementById("name").value = "";
  document.getElementById("price").value = "";
  document.getElementById("screen").value = "";
  document.getElementById("backCamera").value = "";
  document.getElementById("frontCamera").value = "";
  document.getElementById("image").value = "";
  document.getElementById("desc").value = "";
  document.getElementById("type").value = "";
}

function clearThongBao() {
  let tbList = document.querySelectorAll(".sp-thongbao");
  tbList.forEach((ele) => {
    ele.style.display = "none";
  });
}

function renderDSSP(spArr) {
  var contentHTML = "";
  for (var i = 0; i < spArr.length; i++) {
    var sp = spArr[i];

    var trContent = ` <tr>
      <td>${sp.name}</td>
      <td>${sp.price}</td>
      <td>${sp.screen}</td>
      <td>${sp.backCamera}</td>
      <td>${sp.frontCamera}</td>
      <td><img src="${sp.img}" alt="${sp.img}" style="width: 80px"></td>
      <td>${sp.desc}</td>
      <td>${sp.type}</td>
      <td>
      <button onclick="removeProduct('${sp.id}')" class="btn btn-danger">Xoá</button>
      <button onclick="editProduct('${sp.id}')"
      class="btn btn-warning" id="btnThem" data-toggle="modal" data-target="#myModal">Sửa</button>
      </td>
      </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTriTheoType(type, dssp) {
  var newDSSP = [];
  for (var index = 0; index < dssp.length; index++) {
    var sp = dssp[index];
    if (sp.type == type || sp.type == type) {
      newDSSP.push(sp);
    }
    if (type == "") {
      newDSSP = dssp;
    }
  }
  return newDSSP;
}

function checkEmptyKey2(phone) {
  let isValid = true;
  Object.keys(phone).forEach((key) => {
    isValidKey = validation.kiemTraRong(
      phone[key],
      `tb${key}`,
      "vui lòng điền thông tin"
    );
    isValid &= isValidKey;
  });
  return isValid;
}
