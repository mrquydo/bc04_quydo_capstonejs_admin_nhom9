const BASE_URL = "https://62fb79d0abd610251c09c2f7.mockapi.io";

let productUpdateId = null;

function getDSSP() {
  axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  })
    .then(function (res) {
      renderDSSP(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}
getDSSP();

function addProduct() {
  var newProduct = layThongTinTuForm();
  let isEmpty = checkEmptyKey2(newProduct);
  let isValidPrice = validation.kiemTraSo(
    newProduct.price,
    "tbprice",
    "chỉ nhập số"
  );
  if (isEmpty && isValidPrice) {
    axios({
      url: `${BASE_URL}/Products/`,
      method: "POST",
      data: newProduct,
    })
      .then(function (res) {
        getDSSP();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

function removeProduct(id) {
  axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "DELETE",
  })
    .then(function () {
      getDSSP();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function editProduct(id) {
  clearThongBao();
  productUpdateId = id;
  axios({
    url: `${BASE_URL}/Products/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      console.log(err);
    });
}

function updateProduct() {
  let id = productUpdateId;
  let newProduct = layThongTinTuForm();
  let isEmpty = checkEmptyKey2(newProduct);
  let isValidPrice = validation.kiemTraSo(
    newProduct.price,
    "tbprice",
    "chỉ nhập số"
  );
  let isValid = isEmpty && isValidPrice;

  if (isValid) {
    axios({
      url: `${BASE_URL}/Products/${id}`,
      method: "PUT",
      data: newProduct,
    })
      .then(function (res) {
        getDSSP();
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

function searchProduct() {
  console.log("type: ", type);

  axios({
    url: `${BASE_URL}/Products`,
    method: "GET",
  })
    .then(function (res) {
      let type = document.getElementById("searchName").value;
      let newDSSP = timKiemViTriTheoType(type, res.data);
      renderDSSP(newDSSP);
    })
    .catch(function (err) {
      console.log(err);
    });
}
